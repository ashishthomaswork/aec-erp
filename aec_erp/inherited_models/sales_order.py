from odoo.exceptions import UserError, AccessError
from odoo import api, fields, models, SUPERUSER_ID, _

class opt_sale_line(models.Model):
    _name = 'opt.sale.line'

    _inherit = 'sale.order.line'

    order_id = fields.Many2one('sale.order','Order ID')



class SaleOrder(models.Model):
    _inherit='sale.order'

    opt_order_lines = fields.One2many('opt.sale.line','order_id','Optional Order Lines')

    @api.model
    def create(self, vals):
        if vals.get('name', _('New')) == _('New'):
            if 'is_a_quot' in vals and vals.get('is_a_quot') == True:
                vals['name'] = self.env['ir.sequence'].next_by_code('quot.code') or _('New')
            else:
                sales_enq = self.env['sales.enquiry'].browse(vals['sales_enquiry_id'])
                vals['name'] = sales_enq.name+'/'+ self.env['ir.sequence'].next_by_code('sale.order') or _('New')

        # Makes sure partner_invoice_id', 'partner_shipping_id' and 'pricelist_id' are defined
        if any(f not in vals for f in ['partner_invoice_id', 'partner_shipping_id', 'pricelist_id']):
            partner = self.env['res.partner'].browse(vals.get('partner_id'))
            addr = partner.address_get(['delivery', 'invoice'])
            vals['partner_invoice_id'] = vals.setdefault('partner_invoice_id', addr['invoice'])
            vals['partner_shipping_id'] = vals.setdefault('partner_shipping_id', addr['delivery'])
            vals['pricelist_id'] = vals.setdefault('pricelist_id',
                                                   partner.property_product_pricelist and partner.property_product_pricelist.id)
        result = super(SaleOrder, self).create(vals)
        return result



    @api.depends('order_line.price_total','add_charges','discount')
    def _amount_all(self):
        """
        Compute the total amounts of the SO.
        """
        for order in self:
            amount_untaxed = amount_tax = 0.0
            for line in order.order_line:
                if  line.total_price_fc > 0 :
                    amount_untaxed += line.total_price_fc
                else:
                    amount_untaxed += line.price_subtotal

                amount_tax += line.price_tax

            order.update({
                'amount_untaxed': amount_untaxed,
                'amount_tax': amount_tax,
                'amount_total': amount_untaxed + amount_tax + order.add_charges - order.discount,
            })



    order_type=fields.Selection([('sales','Sales Order'),('service','Service Order'),('main','Maintenance Order'),('direct','Direct Order'),('project','Project')],'Order Type')

    sales_enquiry_id = fields.Many2one('sales.enquiry')
    preface_sentence = fields.Text('Preface Sentence')
    attention = fields.Char('Attention')
    subject = fields.Char('Subject')

    serv_main = fields.Boolean('Service Maintenance')


    curr_exchange_rate =fields.Float('Currency Exchange Rate')

    own_currency = fields.Many2one('res.currency','Currency')
    shipment_mode = fields.Char('Shipment Mode')

    payment_mode = fields.Char('Payment Mode')

    add_clause = fields.Text('Additional Clause')

    add_charges = fields.Float('Additional Charges')

    discount = fields.Float('Discount')

    delivery_period = fields.Text('Delivery Period')

    add_po = fields.Many2one('purchase.order','Add Supplier order')

    client_po = fields.Char('Client PO#')

    is_a_quot = fields.Boolean('Is a Quotation')

    @api.onchange('add_po')
    def purchase_order_change(self):
        if not self.add_po:
            return {}


        new_lines = self.env['po.so.lines']
        for line in self.add_po.order_line:

            data = {
                'name':line.name,
                'product_id': line.product_id.id,
                'unit': line.product_uom.id,
                'qty': line.product_qty,
                'part_no': line.part_no,
                'partner_id':line.order_id.partner_id.id,
                'po_id':line.order_id.id

            }
            new_line = new_lines.new(data)

            new_lines += new_line

        check= [x for x in self.po_so_line if self.add_po == x.po_id]
        if len(check) <1:
            self.po_so_line += new_lines

        self.add_po = False
        return {}




    amount_total = fields.Monetary(string='Net Total Amount', store=True, readonly=True, compute='_amount_all', track_visibility='always')


    po_so_line  = fields.One2many('po.so.lines','order_id','Supplier Info')


    @api.onchange('sales_enquiry_id')
    def onchange_se(self):
        for record in self:
            record.partner_id = record.sales_enquiry_id.partner_id.id
            record.subject = record.sales_enquiry_id.subject
            new_lines = self.env['sale.order.line']
            for line in self.sales_enquiry_id.sales_enquiry_lines:
                data = {
                    'name': line.product_id.name,
                    'product_id':line.product_id.id,
                    'product_uom_qty': line.qty,
                    'supplier_id':line.partner_id.id,

                    'product_uom': line.product_id.uom_id.id,

                }
                new_line = new_lines.new(data)

                new_lines += new_line

            self.order_line = new_lines


    @api.onchange('add_charges','discount')
    def onchange_add_charges(self):
        for record in self:
            record.amount_total = record.amount_untaxed + record.add_charges - record.discount

class SaleOrderLine(models.Model):
    _inherit='sale.order.line'

    part_no = fields.Char('Part #')

    supplier_id  = fields.Many2one('res.partner','Supplier')

    @api.depends('unit_price_fc','product_uom_qty')
    def _amount_unit_fc(self):
        for record in self:
            #if record.order_id.curr_exchange_rate > 0:
                #record.unit_price_fc =(record.price_unit / record.order_id.curr_exchange_rate)
            record.total_price_fc  =record.unit_price_fc  * record.product_uom_qty

    unit_price_fc = fields.Float('Unit Price')

    total_price_fc = fields.Float('Total Price',compute='_amount_unit_fc')

    def _get_line_numbers(self):
        line_num = 1
        if self.ids:
            first_line_rec = self.browse(self.ids[0])

            for line_rec in first_line_rec.order_id.order_line:
                line_rec.line_no = line_num
                line_num += 1

    line_no = fields.Integer(compute='_get_line_numbers', string='Serial Number', readonly=False, default=False)

    @api.onchange('product_uom', 'product_uom_qty')
    def product_uom_change(self):
        if not self.product_uom or not self.product_id:
            self.price_unit = 0.0
            return


class po_so_lines(models.Model):
    _name = 'po.so.lines'

    partner_id  = fields.Many2one('res.partner','Supplier')

    po_id = fields.Many2one('purchase.order','PO')

    name = fields.Char('Description')

    product_id = fields.Many2one('product.product','Item#')

    part_no = fields.Char('Part #')

    qty = fields.Float('Quantity')

    unit  = fields.Many2one('product.uom','Unit')

    order_id =  fields.Many2one('sale.order','Order')

    # @api.onchange('unit_price_fc','product_uom_qty')
    # def onchange_unit_fc(self):
    #     for record in self:
    #         record.total_price_fc = record.unit_price_fc*record.product_uom_qty