from odoo.exceptions import UserError, AccessError
from odoo import api, fields, models, SUPERUSER_ID, _

class Users(models.Model):
    _inherit = 'res.users'

    is_a_salesperson = fields.Boolean('Salesperson',default=True)