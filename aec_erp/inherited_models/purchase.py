from odoo.exceptions import UserError, AccessError
from odoo import api, fields, models, SUPERUSER_ID, _
from datetime import datetime



class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'


    sale_order_id = fields.Many2one('sale.order','Sale Order')

    @api.depends('order_line.price_total')
    def _amount_all(self):
        for order in self:
            amount_untaxed = amount_tax = 0.0
            for line in order.order_line:
                amount_untaxed += line.price_subtotal
                amount_tax += line.price_tax
            order.update({
                'amount_untaxed': order.currency_id.round(amount_untaxed),
                'amount_tax': order.currency_id.round(amount_tax),
                'amount_total': amount_untaxed + amount_tax + order.add_charges,
            })

    @api.onchange('sale_order_id')
    def onchange_sale_order(self):
        for record in self:
            record.sales_enquiry_id = record.sale_order_id.sales_enquiry_id.id
            record.date_order = record.sale_order_id.date_order
            record.client_id = record.sale_order_id.partner_id.id

            new_lines = self.env['purchase.order.line']
            for line in self.sale_order_id.order_line:
                data = {
                    'name': line.product_id.name,
                    'product_id': line.product_id.id,
                    'product_qty': line.product_uom_qty,
                  #  'supplier_id': line.partner_id.id,

                    'product_uom': line.product_id.uom_po_id.id,

                }
                new_line = new_lines.new(data)

                new_lines += new_line

            self.order_line = new_lines

    @api.onchange('add_charges')
    def onchange_add_charges(self):
        for record in self:
            record.amount_total = record.amount_untaxed + record.add_charges

    amount_total = fields.Monetary(string='Net Total ', store=True, readonly=True, compute='_amount_all')
    sales_enquiry_id = fields.Many2one('sales.enquiry')
    preface_sentence = fields.Text('Preface Sentence')
    penalty_clause = fields.Text('Penalty Clause')
    commission = fields.Float('Commission')

    client_id = fields.Many2one('res.partner','Client')
    attention = fields.Char('Attention')
    due_date = fields.Date('Due Date')

    shipment_mode = fields.Char('Shipment Mode')

    payment_mode = fields.Char('Payment Mode')

    add_clause = fields.Text('Additional Clause')

    add_charges = fields.Float('Additional Charges')

    delivery_period = fields.Text('Delivery Period')

class PurchaseOrderline(models.Model):
    _inherit = 'purchase.order.line'

    date_planned = fields.Datetime(default=datetime.today())

    part_no = fields.Char('Part #')

    def _get_line_numbers(self):
        line_num = 1
        if self.ids:
            first_line_rec = self.browse(self.ids[0])

            for line_rec in first_line_rec.order_id.order_line:
                line_rec.line_no = line_num
                line_num += 1

    line_no = fields.Integer(compute='_get_line_numbers', string='Serial Number', readonly=False, default=False)

    unit_price_fc = fields.Float('Unit Price FC')

    total_price_fc = fields.Float('Total Price FC')

    @api.onchange('unit_price_fc','product_qty')
    def onchange_unit_fc(self):
        for record in self:
            record.total_price_fc = record.unit_price_fc*record.product_qty


#