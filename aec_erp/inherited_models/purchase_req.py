from odoo.exceptions import UserError, AccessError
from odoo import api, fields, models, SUPERUSER_ID, _
from datetime import datetime



class PurchaseRequisition(models.Model):
    _inherit = 'purchase.requisition'

    name = fields.Char('Supplier RFQ')

    sales_enquiry_id = fields.Many2one('sales.enquiry')

    @api.multi
    def action_rfq_send(self):
        '''
        This function opens a window to compose an email, with the edi purchase template message loaded by default
        '''
        self.ensure_one()
        ir_model_data = self.env['ir.model.data']
        try:
            template_id = ir_model_data.get_object_reference('aec_erp', 'email_template_preq')[1]

        except ValueError:
            template_id = False

        try:
            compose_form_id = ir_model_data.get_object_reference('mail', 'email_compose_message_wizard_form')[1]
        except ValueError:
            compose_form_id = False
        ctx = dict(self.env.context or {})
        ctx.update({
            'default_model': 'purchase.requisition',
            'default_res_id': self.ids[0],
            'default_use_template': bool(template_id),
            'default_template_id': template_id,
            'default_composition_mode': 'comment',
         #  'custom_layout': "purchase.mail_template_data_notification_email_purchase_order",
        #'purchase_mark_rfq_sent': True,
            'force_email': True
        })
        return {
            'name': _('Compose Email'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'mail.compose.message',
            'views': [(compose_form_id, 'form')],
            'view_id': compose_form_id,
            'target': 'new',
            'context': ctx,
        }

    @api.onchange('sales_enquiry_id')
    def onchange_sales_enquiry(self):
        for record in self:
            record.due_date = record.sales_enquiry_id.closing_date


    preface_sentence = fields.Text('Preface Sentence')
    attention = fields.Char('Attention')
    due_date = fields.Date('Due Date')

class PurchaseRequisitionline(models.Model):
    _inherit = 'purchase.requisition.line'

    name = fields.Char('Description')


    part_no = fields.Char('Part #')

    def _get_line_numbers(self):
        line_num = 1
        if self.ids:
            first_line_rec = self.browse(self.ids[0])

            for line_rec in first_line_rec.requisition_id.line_ids:
                line_rec.line_no = line_num
                line_num += 1

    line_no = fields.Integer(compute='_get_line_numbers', string='Serial #', readonly=False, default=False)

