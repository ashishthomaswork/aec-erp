from odoo.exceptions import UserError, AccessError
from odoo import api, fields, models, SUPERUSER_ID, _

class cost_sheet_rfq(models.Model):
    _name='cost.sheet.rfq'

    name = fields.Char('Name')


    sales_enquiry_id = fields.Many2one('sales.enquiry', 'Enquiry Ref')

    purchase_id = fields.Many2one(
        comodel_name='purchase.order',
        string='Add Purchase Order',

        help='Encoding help. When selected, the associated purchase order lines are added to the cost sheet rfq. Several PO can be selected.'
    )

    cost_sheet_rfq_lines = fields.One2many('cost.sheet.rfq.line', 'cost_sheet_rfq_id', 'Cost Sheet RFQ')

    cost_sheet_id  = fields.Many2one('cost.sheet','Cost Sheet')

    operational_cost = fields.Float('Operational Cost')

    @api.onchange('cost_sheet_id')
    def cost_sheet_change(self):
        if not self.cost_sheet_id:
            return {}

        self.operational_cost = self.cost_sheet_id.total_kd_with_other





    state = fields.Selection([('draft', 'Draft'), ('manag_appr', 'Management Approved')], 'State', default='draft')

    @api.multi
    def manag_approv(self):
        self.write({'state': 'manag_appr'})


    @api.onchange('purchase_id')
    def purchase_order_change(self):
        if not self.purchase_id:
            return {}


        new_lines = self.env['cost.sheet.rfq.line']
        for line in self.purchase_id.order_line:
            data = {

                'product_id' : line.product_id.id,
                'uom_id': line.product_uom.id,
                'qty': line.product_qty,
                'unit_price':line.price_unit,
                'total_price': line.price_unit*line.product_qty
            }
            new_line = new_lines.new(data)

            new_lines += new_line

        self.cost_sheet_rfq_lines += new_lines

        self.purchase_id = False
        return {}

class cost_sheet_rfq_line(models.Model):
    _name='cost.sheet.rfq.line'

    name = fields.Char('Name')

    cost_sheet_rfq_id = fields.Many2one('cost.sheet.rfq','Cost Sheet RFQ')

    product_id = fields.Many2one('product.product','Product')

    qty = fields.Float('Quantity')

    uom_id  =fields.Many2one('product.uom','uom')

    unit_price = fields.Float('Unit Price')

    total_price = fields.Float('Total Price')

