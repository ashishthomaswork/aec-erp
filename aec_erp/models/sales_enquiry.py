from odoo.exceptions import UserError, AccessError
from odoo import api, fields, models, SUPERUSER_ID, _

class AECDept(models.Model):
    _name = 'aec.dept'
    _description = 'AEC Departments'

    name = fields.Char('Department',required=True)


class SalesEnquiry(models.Model):
    _name = 'sales.enquiry'
    _description = 'Sales Enquiry'

    @api.multi
    def action_rfq_send(self):
        '''
        This function opens a window to compose an email, with the edi purchase template message loaded by default
        '''
        self.ensure_one()
        ir_model_data = self.env['ir.model.data']
        try:
          template_id = ir_model_data.get_object_reference('aec_erp', 'email_template_sales_enq')[1]

        except ValueError:
            template_id = False
        #template_id = False
        try:
            compose_form_id = ir_model_data.get_object_reference('mail', 'email_compose_message_wizard_form')[1]
        except ValueError:
            compose_form_id = False
        ctx = dict(self.env.context or {})
        ctx.update({
            'default_model': 'sales.enquiry',
            'default_res_id': self.ids[0],
            'default_use_template': bool(template_id),
            'default_template_id': template_id,

            'default_composition_mode': 'comment',
            #  'custom_layout': "purchase.mail_template_data_notification_email_purchase_order",
            # 'purchase_mark_rfq_sent': True,
            'force_email': True
        })
        return {
            'name': _('Compose Email'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'mail.compose.message',
            'views': [(compose_form_id, 'form')],
            'view_id': compose_form_id,
            'target': 'new',
            'context': ctx,
        }

    @api.multi
    def comp_rfq_count(self):
        for each in self:
            document_ids = self.env['purchase.requisition'].search([('sales_enquiry_id', '=', each.id)])
            each.rfq_count = len(document_ids)

    @api.multi
    def comp_quot_count(self):
        for each in self:
            document_ids = self.env['sale.order'].search([('sales_enquiry_id', '=', each.id),('is_a_quot','=',True)])
            each.quot_count = len(document_ids)

    @api.multi
    def comp_cost_sheet_count(self):
        for each in self:
            document_ids = self.env['cost.sheet'].search([('sales_enquiry_id', '=', each.id)])
            each.cost_sheet_count = len(document_ids)

    @api.multi
    def comp_cost_sheet_rfq_count(self):
        for each in self:
            document_ids = self.env['cost.sheet.rfq'].search([('sales_enquiry_id', '=', each.id)])
            each.cost_sheet_rfq_count = len(document_ids)

    # @api.multi
    # def document_view(self):
    #     self.ensure_one()
    #     domain = [
    #         ('employee_ref', '=', self.id)]
    #     return {
    #         'name': _('Documents'),
    #         'domain': domain,
    #         'res_model': 'hr.employee.document',
    #         'type': 'ir.actions.act_window',
    #         'view_id': False,
    #         'view_mode': 'tree,form',
    #         'view_type': 'form',
    #         'help': _('''<p class="oe_view_nocontent_create">
    #                            Click to Create for New Documents
    #                         </p>'''),
    #         'limit': 80,
    #         'context': "{'default_employee_ref': '%s'}" % self.id
    #     }
    #
    # document_count = fields.Integer(compute='_document_count', string='# Documents')

    name = fields.Char('#', copy=False)

    se_date = fields.Date('Date')

    partner_id = fields.Many2one('res.partner', 'Client')

    partner_ref_no = fields.Char('Client Ref #')

    closing_date = fields.Date('Closing Date')

    client_enq_ref =fields.Char('Client Enquiry Reference #')

    type_of_enquiry = fields.Selection([('enquiries', 'Enquires'), ('RFQ', 'RFQ'), ('RFP', 'RFP'), ('Other', 'Other')],
                                       'Type of Enquiry')

    subject = fields.Text('Subject')

    bank_guar = fields.Boolean('Bank Guarantee')

    aec_dept_id  = fields.Many2one('aec.dept','Department')

    salesperson_id  = fields.Many2one('res.users','Sales Person')

    sales_enquiry_lines = fields.One2many('sales.enquiry.line','sales_enquiry_id','Lines')

    rfq_count = fields.Integer('RFQ Count',compute='comp_rfq_count')

    quot_count = fields.Integer('Quotation Count', compute='comp_quot_count')

    cost_sheet_count = fields.Integer('Cost Sheet Count', compute='comp_cost_sheet_count')

    cost_sheet_rfq_count = fields.Integer('Cost Sheet RFQ Count', compute='comp_cost_sheet_rfq_count')



    @api.multi
    def request_initial_bg(self):
        return True
    @api.multi
    def create_rfq(self):
        for record in self:
            for line in record.sales_enquiry_lines:
                search_rfq = self.env['purchase.requisition'].search([('vendor_id','=',line.partner_id.id),('sales_enquiry_id','=',record.id)])
                if not search_rfq:
                    vals={}
                    vals['vendor_id']= line.partner_id.id
                    vals['sales_enquiry_id']=record.id

                    rfq_id = self.env['purchase.requisition'].create(vals)
                    vals_line={}
                    vals_line['product_id'] = line.product_id.id
                    vals_line['name']=line.product_id.name
                    vals_line['product_qty']=line.qty
                    vals_line['requisition_id']=rfq_id.id


                    vals_line['product_uom_id']=line.product_id.uom_po_id.id
                    vals_line['price_unit']=0.0
                    self.env['purchase.requisition.line'].create(vals_line)
                else:

                    vals_line = {}
                    vals_line['product_id'] = line.product_id.id
                    vals_line['name'] = line.product_id.name
                    vals_line['product_qty'] = line.qty
                    vals_line['requisition_id'] = search_rfq.id
                    vals_line['price_unit']=0.0

                    vals_line['product_uom_id']=line.product_id.uom_po_id.id

                    self.env['purchase.requisition.line'].create(vals_line)




    def initial(self,sales_name):
        saleinitial = ''
        if (len(sales_name) == 0):
            saleinitial = ''
        saleinitial = sales_name[0].upper()

        for i in range(1, len(sales_name) - 1):
            if (sales_name[i] == ' '):
                saleinitial = saleinitial + sales_name[i + 1].upper()
        return saleinitial


    @api.model
    def create(self,vals):
        x = self.env['ir.sequence'].next_by_code('sales.enquiry') or '/'
        dept = vals.get('aec_dept_id')
        dept_name=''
        sales_name=''
        if dept:
            dept_obj = self.env['aec.dept'].browse(dept)
            dept_name = dept_obj.name
        salesper = vals.get('salesperson_id')
        if salesper:
            salesper_obj = self.env['res.users'].browse(salesper)


            #sales_name = self.initial(salesper_obj.name)
            sales_name = salesper_obj.name
            sales_name = sales_name[0:2]
        vals['name'] = dept_name+"/"+sales_name+"/"+x


        return super(SalesEnquiry, self).create(vals)


class sales_enquiry_line(models.Model):
    _name = 'sales.enquiry.line'

    name = fields.Char('Name')

    product_id = fields.Many2one('product.product','Item')

    qty = fields.Float('Quantity')

    partner_id = fields.Many2one('res.partner','Supplier')

    attention  = fields.Char('Attention')

    sales_enquiry_id = fields.Many2one('sales.enquiry','Sales Enquiry')



