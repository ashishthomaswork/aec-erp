from odoo.exceptions import UserError, AccessError
from odoo import api, fields, models, SUPERUSER_ID, _


class cost_sheet(models.Model):
    _name = 'cost.sheet'

    _description = 'Cost Sheet'

    name = fields.Char('Our Ref')

    cls_date = fields.Date('Closing Date')

    supplier_id = fields.Many2one('res.partner','Supplier')

    sales_enquiry_id = fields.Many2one('sales.enquiry', 'Enquiry Ref')

    sup_ref = fields.Char('Supplier Ref')

    validity = fields.Integer('Validity(Days)')

    delivery = fields.Integer('MFR Delivery(Days)')

    client_id = fields.Many2one('res.partner','Client')

    currency_id = fields.Many2one('res.currency','Currency')


    @api.onchange('sales_enquiry_id')
    def onchange_sales_enq(self):
        for record in self:
            record.client_id = record.sales_enquiry_id.partner_id.id

            new_lines = self.env['cost.sheet.lines']
            for line in self.sales_enquiry_id.sales_enquiry_lines:
                data = {
                    'name': line.product_id.name,
                                   'qty_ea': line.qty,

                    'supplier_id': line.partner_id.id,


                }
                new_line = new_lines.new(data)

                new_lines += new_line

            self.cost_sheet_lines = new_lines


    cost_sheet_lines = fields.One2many('cost.sheet.lines', 'cost_sheet_id', 'Cost Sheet')



    state = fields.Selection([('draft','Draft'),('manag_appr','Management Approved')],'State',default='draft')


    @api.multi
    def manag_approv(self):
        self.write({'state':'manag_appr'})

    @api.depends('cost_sheet_lines')
    def comp_exworks(self):
        for record in self:
            val = 0
            for line in record.cost_sheet_lines:
                val = val + line.total_amt_fc
            record.exworks_value = val

    @api.depends('cost_sheet_lines')
    def comp_rate_parent(self):
        for record in self:
            val = 0
            count = 0
            for line in record.cost_sheet_lines:
                val = val + line.rate
                count += 1
            if count > 1:
                rate = val /count
            else:
                rate  = 1

            record.exworks_value = rate

    @api.depends('cost_sheet_lines')
    def comp_exworks_kd(self):
        for record in self:
            val = 0
            for line in record.cost_sheet_lines:
                val = val + line.total_amt_kd
            record.exworks_kd_value = val

    exworks_value = fields.Float('EXWORKS VALUE', compute='comp_exworks')

    exworks_kd_value = fields.Float('EXWORKS KD VALUE',compute='comp_exworks_kd')

    export_packing = fields.Float('Export Packing & Handling')

    document_amt = fields.Float('Document')

    extra_amt = fields.Float('Extra')

    freight_type  = fields.Selection([('Sea','Sea'),('Air','Air'),('Land','Land')],'Freight By')
    freight_amt = fields.Float('Freight Amt')

    @api.depends('exworks_value', 'export_packing', 'document_amt', 'extra_amt', 'freight_amt')
    def comp_total_cf_value(self):
        for record in self:
            record.total_cf_kd_val = record.exworks_value + record.export_packing + record.document_amt + record.extra_amt + record.freight_amt

    total_cf_kd_val = fields.Float('TOTAL C&F KUWAIT VALUE', compute='comp_total_cf_value')

    exhange_rate = fields.Float('Rate @',compute='comp_rate_parent')

    @api.depends('total_cf_kd_val', 'exhange_rate')
    def comp_total_kd_val(self):
        for record in self:
            record.total_kd_val = round(record.total_cf_kd_val * record.exhange_rate,0)

    total_kd_val = fields.Float('KD', compute='comp_total_kd_val')

    p_handl = fields.Float('P/Handl')

    expense = fields.Float('Expense')

    c_duty = fields.Float('C.Duty')

    trans = fields.Float('Transpt.')

    bg_val = fields.Float('BG')

    lc_val = fields.Float('L/C')

    finance = fields.Float('Finance')

    other_tp = fields.Float('Other / TP')

    markup = fields.Float('Mark Up %')

    tons = fields.Float('Tons')

    expense_value = fields.Float('Expense Value')

    custom_value  = fields.Float('Custom value')

    truck = fields.Float('Truck')

    @api.depends('total_kd_val','bg_val')
    def compute_bg_months(self):
        for record in self:
            record.bg_months = round((record.total_kd_val)*0.1*0.00125*(record.bg_val),0)

    @api.depends('total_kd_val', 'lc_val')
    def compute_lc_months(self):
        for record in self:
            record.lc_months = round((record.total_kd_val) * 0.00125 * (record.lc_val), 0)

    @api.depends('total_kd_val', 'finance')
    def compute_fin_percent(self):
        for record in self:
            record.fin_percent = round((record.total_kd_val) * 0.1 *(90/365)*((record.finance)/100.0), 0)

    @api.depends('total_kd_val', 'markup')
    def compute_markup_value(self):
        for record in self:
            record.markup_value = round((record.total_kd_val) * ((record.markup)/100.0), 1)

    bg_months = fields.Float('Months',compute='compute_bg_months')

    lc_months =fields.Float('Months',compute='compute_lc_months')

    fin_percent = fields.Float('Percent',compute='compute_fin_percent')

    tp_value  = fields.Float('TP Value')

    markup_value  = fields.Float('Markup value',compute='compute_markup_value')
    bank_percent = fields.Float('Bank Percent')

    @api.depends('exworks_kd_value', 'bank_percent')
    def comp_bank_gar(self):
        for record in self:
            record.bank_gar = round((record.exworks_kd_value) * ((record.bank_percent) / 100.0), 0)


    bank_gar = fields.Float('Bank GUARANTEE',compute='comp_bank_gar')

    comm_percent = fields.Float('Commission Percent')

    @api.depends('total_kd_val', 'comm_percent')
    def comp_comm(self):
        for record in self:
            record.commission = round((record.total_kd_val) * ((record.comm_percent) / 100.0), 0)

    commission = fields.Float('Commission', compute='comp_comm')




    @api.depends('total_kd_val','tons','expense_value','custom_value','truck','bg_months','lc_months','fin_percent','tp_value','markup_value')
    def comp_total_kd_with_other(self):
        for record in self:
            record.total_kd_with_other = record.total_kd_val+record.tons+record.expense_value+record.custom_value+record.truck+record.bg_months+record.lc_months+record.fin_percent+record.tp_value+record.markup_value

    total_kd_with_other = fields.Float('KD',compute='comp_total_kd_with_other')

    @api.depends('total_kd_with_other','exworks_value')
    def comp_fractions(self):
        for record in self:
            if record.exworks_value > 0:
                record.fractions = (record.total_kd_with_other)/record.exworks_value
    fractions = fields.Float('Fractions',compute='comp_fractions',digits=(16,8))


    @api.model
    def create(self,vals):
        vals['name']=self.env['ir.sequence'].next_by_code('cost.sheet') or '/'

        return super(cost_sheet, self).create(vals)

class cost_sheet_lines(models.Model):
    _name = 'cost.sheet.lines'

    name = fields.Char('Description')

    supplier_id = fields.Many2one('res.partner','Supplier')

    cost_sheet_id = fields.Many2one('cost.sheet', 'Cost Sheet')

    unit_fc = fields.Float('Unit price (FC)')


    rate = fields.Float('Rate')

    qty_ea = fields.Float('Quantity (EA)')

    def _get_line_numbers(self):
        line_num = 1
        if self.ids:
            first_line_rec = self.browse(self.ids[0])

            for line_rec in first_line_rec.cost_sheet_id.cost_sheet_lines:
                line_rec.line_no = line_num
                line_num += 1

    line_no = fields.Integer(compute='_get_line_numbers', string='Serial No', readonly=False, default=False)

    @api.depends('unit_fc', 'qty_ea')
    def comp_total_amt_fc(self):
        for record in self:
            record.total_amt_fc = record.unit_fc * record.qty_ea

    total_amt_fc = fields.Float('Total Amount (FC)', compute='comp_total_amt_fc')
    @api.depends('rate','unit_fc')
    def comp_unit_kd(self):
        for record in self:
            if record.cost_sheet_id:
               # record.unit_kd =round((record.cost_sheet_id.fractions*record.unit_fc),0)
                record.unit_kd = round((record.rate*record.unit_fc),0)

    unit_kd = fields.Float('Unit Price (KD)',compute='comp_unit_kd')

    @api.depends('unit_kd','qty_ea')
    def comp_total_mat_kd(self):
        for record in self:
            record.total_amt_kd = record.unit_kd * record.qty_ea

    total_amt_kd = fields.Float('Total Amount (KD)',compute='comp_total_mat_kd')
