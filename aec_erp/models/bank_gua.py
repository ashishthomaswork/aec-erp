from odoo.exceptions import UserError, AccessError
from odoo import api, fields, models, SUPERUSER_ID, _

class abkitems(models.Model):
    _name='abkitems'

    name = fields.Char('Name')

class bank_gau(models.Model):
    _name = 'bank.gau'

    name = fields.Char('Name')

    sales_enquiry_id = fields.Many2one('sales.enquiry','Sales Enquiry')

    lang = fields.Selection([('English','English'),('Arabic','Arabic')],'Language')

    bank = fields.Selection([('ABK','ABK Bank'),('Gulf','Gulf Bank'),('Burgan','Burgan Bank')],'Bank')

    gau_date = fields.Date('Date')

    abk_please = fields.Selection([('Issue','Issue'),('Amend','Amend')],'Please')

    abk_log = fields.Text('Letter of Guarantee')

    abk_items = fields.Many2many('abkitems')

    abk_to_ben = fields.Char('To(The Beneficary)')

    abk_in_name = fields.Char('In name of')

    abk_in_amt = fields.Char('In Amount of')

    abk_currency = fields.Char('Currency')

    abk_in_respect = fields.Char('In respect of')

    abk_from = fields.Char('Valid for a period of From')

    abk_to = fields.Char('To')

    abk_charge = fields.Char('Kindly Debit your charges to our account no')

    abk_name = fields.Char('Applicants Name')

    abk_address = fields.Char('Address')

    abk_tel_fax = fields.Char('Telephone/Fax')



